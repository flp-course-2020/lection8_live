import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Step02MonadLawsExamples extends App {
  import Monad._
  import MonadInstances._

  object ListE {

    val repeat3: Int => List[Int] = x => x :: x :: x :: Nil

    val sqr: Int => List[Int] = x => (x * x) :: Nil

    val toStr: Int => List[String] = x => x.toString ++ x.toString :: Nil

//    val l = pure[List, Int] _ >=> repeat3
//    val r = repeat3 >=> pure[List, Int]

//
//    println(l(1))
//    println(r(1))
//    println(l(1) == r(1))

//    val comp1 = (repeat3 >=> sqr) >=> toStr
//    val comp2 = repeat3 >=> (sqr >=> toStr)

//    println(comp1(2))
//    println(comp2(2))
//    println(comp1(2) == comp2(2))
  }

  object WriterE {

    val f: Int => Writer[Int] = x => Writer(x * x, "Возвели в квадрат" :: Nil)

    val g: Int => Writer[Int] = x => Writer(x * 2, "Умножили на 2" :: Nil)

    val c: Int => Writer[Int] = x => Writer(x % 3, "Взяли остаток от деления на 3" :: Nil)

    val wp1 = pure[Writer, Int] _ >=> f
    val wp2 = f >=> pure[Writer, Int]

//    println(wp1(4))
//    println(wp2(4))
//    println(wp1(4) == wp2(4))

    val comp1 = (f >=> g) >=> c
    val comp2 = f >=> (g >=> c)
//
//    println(comp1(2))
//    println(comp2(2))
//    println(comp1(2) == comp2(2))

  }

  object FutureE {

    val f: Int => Future[Int] = x => if (x % 2 == 0) Future.successful(x / 2) else Future.failed(new Exception("Аааа!"))

    val g: Int => Future[String] = x => Future { x.toString }

    val h: String => Future[Boolean] = x => Future { x.length > 10 }

    val fp1 = pure[Future, Int] _ >=> f
    val fp2 = f >=> pure[Future, Int]

//    println(fp1(1))
//    println(fp2(1))

    val comp1 = (f >=> g) >=> h
    val comp2 = f >=> (g >=> h)
//
    println("=" * 20)
    println(comp1(3))
    println(comp2(3))

  }

  FutureE

}
