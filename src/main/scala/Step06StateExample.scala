object Step06StateExample extends App {
  import Monad._
  import Step05State._

  object Fibo {
    type Memo = Map[BigInt, BigInt]

    def fibo(n: BigInt): BigInt = {
      def fiboM(z: BigInt): State[Memo, BigInt] =
        if (z <= 1) pure[State[Memo, *], BigInt](z)
        else for {
          i <- get[Memo].map(_.get(z))
          v <- i.map(pure[State[Memo, *], BigInt]).getOrElse(for {
            r <- fiboM(z - 1)
            s <- fiboM(z - 2)
            t = r + s
            _ <- modify[Memo](x => x + (z -> t))
          } yield t)
        } yield v

      fiboM(n).run(Map())._1
    }

    println(fibo(1))
    println(fibo(2))
    println(fibo(3))
    println(fibo(4))
    println(fibo(5))
    println(fibo(6))
    println(fibo(7))
    println(fibo(1000))
  }

  object Fibo2 {
    type MemoS[A, B, C] = State[Map[A, B], C]
    type MemoF[A, B] = A => MemoS[A, B, B]

    def memo[A, B](f: MemoF[A, B]): MemoF[A, B] = { a =>
      for {
        s <- get[Map[A, B]]
        v = s.get(a)
        r <- v match {
          case Some(b) => pure[MemoS[A, B, *], B](b)
          case None => for {
            b <- f(a)
            _ <- modify[Map[A, B]](_ + (a -> b))
          } yield b
        }
      } yield r
    }

    def fibo1(n: BigInt): BigInt = {
      def loop: MemoF[BigInt, BigInt] = memo { z =>
//        println(s"memo $z")
        if (z == 0 || z == 1) pure[MemoS[BigInt, BigInt, *], BigInt](z)
        else for {
          fz1 <- loop(z - 1)
          fz2 <- loop(z - 2)
        } yield fz1 + fz2
      }

      loop(n).run(Map.empty)._1
    }
  }

  println(Fibo2.fibo1(1000))
}
