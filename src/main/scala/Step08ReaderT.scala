import Step03Reader.Reader
import Step07Transformers._

import scala.concurrent.Future

object Step08ReaderT extends App {
  import Monad._
  import MonadInstances._

  case class ReaderT[F[_], Env, B](run: Env => F[B])

  implicit def readerTMonad[F[_], Env](implicit mf: Monad[F]) = new Monad[ReaderT[F, Env, *]] {
    override def pure[A](a: => A): ReaderT[F, Env, A] = ReaderT(_ => mf.pure(a))
    override def flatMap[A, B](fa: ReaderT[F, Env, A])(f: A => ReaderT[F, Env, B]): ReaderT[F, Env, B] = ReaderT {env =>
      mf.flatMap(fa.run(env))(x => f(x).run(env))
    }
  }

  val a = pure[ReaderT[Option, String, *], Int](42)
  val b = pure[ReaderT[Option, String, *], String]("12")

  val f: Int => Option[String] = x => if (x % 2 == 0) Some(x.toString) else None

  def get[F[_] : Monad, Env] = ReaderT[F, Env, Env]((env: Env) => Monad[F].pure(env))

  val z = for {
    i   <- ReaderT(f)
    env <- get[Option, Int]
    _    = println(env)
  } yield env

  println(z.run(42))

//  val kkk = ReaderT[OptionT[Future, *], Int, Int](x => OptionT(f2(x)))

//  val k = for {
//    i <- ReaderT[OptionT[Future, *], Int, Int](x => OptionT(f2(x)))
//    _  = println(i)
//  } yield i

//  println(z.run(3))
//  val qqq = k.run(3)
//  Thread.sleep(100)
//  println(qqq)

}
